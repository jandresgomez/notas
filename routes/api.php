<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function(){
	return "api notas";
});
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::post('login', 'Api\AuthController@login');
Route::post('signup', 'Api\AuthController@signUp');

Route::group([
	'middleware' => 'auth:api'
], function() {
	Route::get('logout', 'Api\AuthController@logout');
	Route::get('user', 'Api\AuthController@user');

	/*Rutas de las notas*/
	Route::apiResource('notes', 'Api\NotesController');

});
