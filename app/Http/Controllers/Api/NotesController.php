<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Note;

class NotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user_id = $request->user()->id;

        $notes = Note::where("user_id",$user_id)->get();

        return response()->json([
            'notas' => $notes
        ],200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'title' => 'required|string',
            'description' => 'required|string',
            'date' => 'required|date_format:Y-m-d H:i:s'
        ]);


        Note::create([
            'title' => $request->title,
            'description' => $request->description,
            'date' => $request->date,
            'user_id' => $request->user()->id
        ]);

        return response()->json([
            'message' => 'Nota creada!'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $user_id = $request->user()->id;

        $note = Note::find($id);
        if(empty($note)){
            return response()->json([
                'message' => "nota no encontrada"
            ], 400);
        }

        if($note->user_id == $user_id){
            return response()->json([
                'nota' => $note
            ], 200);
        }else{
            return response()->json([
                'message' => "no tiene accesos"
            ], 400);

        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'date' => 'date_format:Y-m-d H:i:s'
        ]);
        $user_id = $request->user()->id;

        $note = Note::find($id);
        if(empty($note)){
            return response()->json([
                'message' => "nota no encontrada"
            ], 400);
        }

        if($note->user_id == $user_id){
            $note->fill($request->all());
            $note->save();
            return response()->json([
                'message' => "nota actualizada"
            ], 200);
        }else{
            return response()->json([
                'message' => "no tiene accesos"
            ], 400);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $user_id = $request->user()->id;

        $note = Note::find($id);

        if($note->user_id == $user_id){

            $note->delete();
            return response()->json([
                'nota' => 'Nota eliminada correctamente'
            ], 200);
        }else{
            return response()->json([
                'message' => "no tiene accesos"
            ], 400);

        }  

    }
}
